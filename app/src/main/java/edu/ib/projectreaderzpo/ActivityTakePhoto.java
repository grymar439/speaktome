package edu.ib.projectreaderzpo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaActionSound;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import com.google.common.util.concurrent.ListenableFuture;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ActivityTakePhoto extends AppCompatActivity {
    PreviewView mCameraView;

    private ListenableFuture<ProcessCameraProvider> cameraProviderFuture;
    private ExecutorService executor = Executors.newSingleThreadExecutor();
    private ImageCapture imageCapture;

    private Button btnCapture;
    byte[] captured;
    ImageButton btnPictureShow;
    boolean isCaptured = false;
    private int rotation = Surface.ROTATION_0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_photo);
        btnPictureShow = findViewById(R.id.btnPictureShow);
        btnPictureShow.setVisibility(View.INVISIBLE);
        btnPictureShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isCaptured){
                Intent intent = new Intent(getApplicationContext(),ImageResultActivity.class);
                Bundle b = new Bundle();
                b.putByteArray("bitmapImage",captured);
                b.putInt("rotation",rotation );
                intent.putExtras(b);
                    btnPictureShow.setVisibility(View.INVISIBLE);
                startActivity(intent);}

            }
        });


        startCamera();


    }

    /**
     * Starting camera
     */

    void startCamera(){
        mCameraView = findViewById(R.id.previewView1);
        btnCapture = findViewById(R.id.btnTakePhoto);
        cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() {
                try {
                    ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                    ActivityTakePhoto.this.bindPreview(cameraProvider);
                } catch (ExecutionException | InterruptedException e) {
                    // This should never be reached.
                }
            }
        }, ContextCompat.getMainExecutor(this));
        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                capturePhoto();
            }
        });

    }

    /**
     *
     * Binding to camera
     */
    private void bindPreview(ProcessCameraProvider cameraProvider) {
        Preview preview = new Preview.Builder()
                .build();

        //Select the camera ->  selecting back camera
        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();

        preview.setSurfaceProvider(mCameraView.getSurfaceProvider());
        // Image capture use case
        imageCapture = new ImageCapture.Builder()
                .build();
        OrientationEventListener orientationEventListener = new OrientationEventListener((Context)this) {
            @Override
            public void onOrientationChanged(int orientation) {

                // Monitors orientation values to determine the target rotation value
                if (orientation >= 45 && orientation < 135) {
                    rotation = Surface.ROTATION_270;
                } else if (orientation >= 135 && orientation < 225) {
                    rotation = Surface.ROTATION_180;
                } else if (orientation >= 225 && orientation < 315) {
                    rotation = Surface.ROTATION_90;
                } else {
                    rotation = Surface.ROTATION_0;
                }

                imageCapture.setTargetRotation(rotation);
            }
        };

        orientationEventListener.enable();
        cameraProvider.unbindAll();
        cameraProvider.bindToLifecycle((LifecycleOwner) this,cameraSelector,preview,imageCapture);
        ContextCompat.getMainExecutor(this);
    }

    /**
     * Captures image as Bitmap to further use
     */
    private void capturePhoto(){
        imageCapture.takePicture(ContextCompat.getMainExecutor(this), new ImageCapture.OnImageCapturedCallback(){
            public void onCaptureSuccess(@NonNull ImageProxy image) {

                captured = imageToByteArr(image);
                System.out.println("ok");
                isCaptured = true;
                MediaActionSound sound = new MediaActionSound();
                sound.play(MediaActionSound.SHUTTER_CLICK);
                image.close();

            }

            public void onError(@NonNull ImageCaptureException exception) {
                System.out.println("nie ok"+exception);
                isCaptured = false;
            }
        }
        );
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                btnPictureShow.setVisibility(View.VISIBLE);
            }
        }, 400);

    }

    /**
     * Clears previous state to avoid TransactionTooLargeException
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.clear();
    }

    /**
     * Method that converts image to byte array
     * @param image
     * @return
     */
    private byte[] imageToByteArr(ImageProxy image){
        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.capacity()];
        buffer.get(bytes);
        Bitmap bitmapImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapImage.compress(Bitmap.CompressFormat.JPEG, 20, stream);
        return stream.toByteArray();

    }

    public void onReturn(View view) {
        finish();
    }
}