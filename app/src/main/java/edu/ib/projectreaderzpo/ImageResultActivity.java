package edu.ib.projectreaderzpo;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.AsyncTaskLoader;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.ColorSpace;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Surface;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class ImageResultActivity extends AppCompatActivity {

    Bitmap imageResult, imgCopy;
    ImageView imagePlaced;
    private int rotation;
    Button applyBtn, simulateBtn, monochromeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_result);
        //defining widgets
        imagePlaced =findViewById(R.id.imgResult);
        applyBtn = findViewById(R.id.btnFilter);
        simulateBtn = findViewById(R.id.btnSimulate);
        monochromeBtn = findViewById(R.id.btnMonochromatic);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        //check if passed bitmap is null -> if true user should try again with photo
        if(extras.getByteArray("bitmapImage")==null){
            Toast.makeText(this, "Failed to proceed, return and try again", Toast.LENGTH_SHORT).show();
        }else {
            byte[] byteArr = extras.getByteArray("bitmapImage");
            rotation = extras.getInt("rotation");
            imageResult = BitmapFactory.decodeByteArray(byteArr, 0, byteArr.length, null);

            imageResult = updateTransform(imageResult);
            imagePlaced.setImageBitmap(imageResult);
            imgCopy = imageResult.copy(imageResult.getConfig(), true);


            applyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(),"Feature in progress", Toast.LENGTH_SHORT).show();
                    /*
                     * Image processing to support users with proteomia (daltonisme)
                     * COMING SOON ...
                     */

//                float[] colorTransform = {
//                        0, 1f, 0, 0, 0,
//                        0, 0, 0f, 0, 0,
//                        0, 0, 0, 0f, 0,
//                        0, 0, 0, 1f, 0};
//
//                ColorMatrix colorMatrix = new ColorMatrix();
//                colorMatrix.setSaturation(0f); //Remove Colour
//                colorMatrix.set(colorTransform); //Apply the Red
//
//                ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
//                Paint paint = new Paint();
//                paint.setColorFilter(colorFilter);
//                Canvas canvas = new Canvas(imageResult);
//                canvas.drawBitmap(imageResult, 0, 0, paint);

                }
            });

            monochromeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ColorMatrix colorMatrix = new ColorMatrix();
                    colorMatrix.setSaturation(0f); //Remove Colour
                    ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
                    Paint paint = new Paint();
                    paint.setColorFilter(colorFilter);
                    Canvas canvas = new Canvas(imgCopy);
                    canvas.drawBitmap(imgCopy, 0, 0, paint);
                    imagePlaced.setImageBitmap(imgCopy);
                }
            });


            simulateBtn.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.Q)
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Feature in progress", Toast.LENGTH_SHORT).show();
                    /**
                     *  Image processing -> additional feature, coming soon...
                     * new CorrectionTask().execute(imgCopy);
                     *
                     *
                     *
                     */

                }
            });

        }
    }

    /**
     * Updates bitmap according to rotation get from prev window
     * @param bitmap
     * @return
     */
    private Bitmap updateTransform(Bitmap bitmap) {
        System.out.println("Update rotation");
        Matrix matrix = new Matrix();
        int flip =0;
        switch (rotation) {
            case Surface.ROTATION_0:
                flip = 90;
                break;

            case Surface.ROTATION_90:
                flip = 0;
                break;

            case Surface.ROTATION_180:
                flip = 270;
                break;

            case Surface.ROTATION_270:
                flip= 180;
                break;

            default:
                break;
        }

        matrix.postRotate((float) flip);
        System.out.println("Rotation:"+flip);

        return Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);
    }


    public void onReturn(View view) {
        finish();
    }

    /**
     * Task to deal with image processing
     * IN PROGRESS
     */

    private class CorrectionTask extends AsyncTask<Bitmap, Void, Bitmap>{




        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected Bitmap doInBackground(Bitmap... bitmaps) {
            ColorCorrector colorCorrector =new ColorCorrector(bitmaps[0]);

            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imagePlaced.setImageBitmap(bitmap);



        }
    }
}