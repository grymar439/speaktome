package edu.ib.projectreaderzpo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.ActivityChooserView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ContentInfoCompat;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.common.model.DownloadConditions;
import com.google.mlkit.nl.translate.TranslateLanguage;
import com.google.mlkit.nl.translate.Translation;
import com.google.mlkit.nl.translate.Translator;
import com.google.mlkit.nl.translate.TranslatorOptions;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    
    ImageButton mCaptureBtn;
    ImageView mImageView;
    ImageButton speechButton;
    TextToSpeech t1;
    Uri image_uri;
    Locale primaryLocale;
    TranslatorOptions options;
    Translator engLoc;
    private int TRANSLATION;
    private boolean granted;
    private final int REQUEST_CODE =1;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Locale primaryLocale = this.getResources().getConfiguration().getLocales().get(0);
        String locale = primaryLocale.getDisplayName();
        Toast.makeText(this,locale ,Toast.LENGTH_SHORT).show();
        mCaptureBtn = findViewById(R.id.startButton);
        speechButton = findViewById(R.id.sayDescription);
        System.out.println(primaryLocale.getLanguage());
        //translator options
        options=
        new TranslatorOptions.Builder()
                .setSourceLanguage(TranslateLanguage.ENGLISH)
                .setTargetLanguage(primaryLocale.getLanguage())
                .build();
         engLoc = Translation.getClient(options);
        DownloadConditions conditions = new DownloadConditions.Builder()
                .requireWifi()
                .build();
        // downloads local language model if needed
        engLoc.downloadModelIfNeeded(conditions)
                .addOnSuccessListener(
                        new OnSuccessListener() {
                            @Override
                            public void onSuccess(Object o) {
                                TRANSLATION = 1;
                                System.out.println("Success");
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                TRANSLATION = 0;
                                System.out.println("Failed");
                            }
                        });


        verifyPermission();
        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS) {
                    int result = t1.setLanguage(Locale.getDefault());
                    if(result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("TTS","Language not supported");
                    }else{
                       speechButton.setEnabled(true);
                    }
                }else{
                    Log.e("TTS", "Initialization failed");
                }
            }
        });
// setting speech button that gives info about app feature that supports people with bad eyesight
        speechButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String toSpeak = "Welcome to app, press camera button to open camera and then press" +
                        " play to recognize text";
                float speed = 0.8f;
                t1.setSpeechRate(speed);
                // checks if language is possible to be find, then translates text if possible and speaks
                if(TRANSLATION==1) {
                    engLoc.translate(toSpeak)
                            .addOnSuccessListener(
                                    new OnSuccessListener() {
                                        @Override
                                        public void onSuccess(Object o) {
                                            t1.speak(o.toString(), TextToSpeech.QUEUE_FLUSH, null);
                                            Toast.makeText(getApplicationContext(), o.toString(),
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    })
                            .addOnFailureListener(
                                    new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                                            Toast.makeText(getApplicationContext(), toSpeak,
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    });

                }
                else{
                    t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                    Toast.makeText(getApplicationContext(), toSpeak, Toast.LENGTH_SHORT).show();
                }

            }
        });

}

    /**
     * Checking permissions
     */
    private void verifyPermission(){
        if(Build.VERSION.SDK_INT <Build.VERSION_CODES.M){
            granted = true;
        }else{
        String[] permissions ={Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if(ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[0])==PackageManager.PERMISSION_GRANTED
        && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[0])==PackageManager.PERMISSION_GRANTED){
            granted =true;
        }else{
            ActivityCompat.requestPermissions(MainActivity.this,permissions,REQUEST_CODE);
        }}
}

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch(requestCode){
            case REQUEST_CODE:
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    if(grantResults[i] ==PackageManager.PERMISSION_DENIED){
                        boolean showRationale = ActivityCompat
                                .shouldShowRequestPermissionRationale(this,permission);
                        if(showRationale){
                            Toast.makeText(this, "Permission denied once or more",
                                    Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(this,"NEVER ASK AGAIN",
                                    Toast.LENGTH_SHORT).show();
                            granted = false;
                        }
                    }else{
                        granted = true;
                    }

                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    /**
     * Opens text recognizer activity
     * @param view
     */

    public void onCameraClicked(View view) {
        if(granted){
        Intent intent = new Intent(this,ActivitySpeaker.class);
        startActivity(intent);}
        else{
            Toast.makeText(this,"Permission not granted", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Opens camera activity for further image processing
     * @param view
     */
    public void onColorInvertClicked(View view) {
        if(granted){
        Intent intent = new Intent(this, ActivityTakePhoto.class);
        startActivity(intent);}
        else{
            Toast.makeText(this,"Permission not granted", Toast.LENGTH_SHORT).show();
        }

    }
}