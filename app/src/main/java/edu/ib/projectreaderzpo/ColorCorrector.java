package edu.ib.projectreaderzpo;



import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.graphics.ColorUtils;

/**
 * Experimental class that bases on article
 * Available on address: https://www.researchgate.net/publication/326626897_Smartphone_Based_Image_Color_Correction_for_Color_Blindness
 * Last use: 15.06.2022
 */

public class ColorCorrector {

    public ColorCorrector(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    private Bitmap imageBitmap;

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    /**
     * Method that applies image correction filter for color blind users
     * (IN PROGRESS)
     * @return
     */

    public void protanopiaCorrector(){
        for (int x = 0; x < imageBitmap.getWidth(); x++) {
            for (int y = 0; y <imageBitmap.getHeight() ; y++) {
                int color = imageBitmap.getPixel(x, y);
                int a = Color.alpha(color);
                int r = Color.red(color);
                int g = Color.green(color);
                int b = Color.blue(color);
                float[] hsl = new float[3];
                ColorUtils.colorToHSL(color,hsl);
                if(Math.abs(r-Color.RED)<10 || Math.abs(g-Color.GREEN)<10){
                    hsl[0] += 0.3*hsl[0];
                    hsl[1] -= hsl[1]*0.1;
                    hsl[2] += hsl[2]*0.25;

                }else{
                    hsl[1] += hsl[1]*0.1;
                    hsl[2] -= hsl[2]*0.1;
                }
                color = Color.HSVToColor(hsl);
                imageBitmap.setPixel(x,y,color);



            }
        }

    }

    /**
     * Method that applies filter to simulate color blind people vision
     * (IN PROGRESS)
     * @return
     */

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void protanopiaSimulator(){
        for (int x = 0; x < imageBitmap.getWidth(); x++) {
            for (int y = 0; y < imageBitmap.getHeight(); y++) {
                int color = imageBitmap.getPixel(x, y);
                int r = Color.red(color);
                int g = Color.green(color);
                int b = Color.blue(color);
                int a = Color.alpha(color);
                // RGB to LMS
                float l = (17.8824f*r + 43.5161f*g +4.11953f*b);
                float m = 3.45565f*r + 27.1554f*g + 3.86714f*b;
                float s = 0.0299566f*r + 0.184309f*g + 1.46709f*b;

                //simulate
                float lP = 2.02344f*m - 2.52581f*s;
                float lM = m;
                float lS = s;

                //convert back with filter enabled
                float rAfter = 0.0809444479f*lP - 0.130504409f*lM + 0.116721066f*lS;
                float gAfter = 0.113614708f*lP - 0.0102485335f*lM + 0.0540193266f*lS;
                float bAfter = -0.000365296938f*lP - 0.00412161469f*lM + 0.693511405f*lS;

                //calculate difference
                float dR = r-rAfter;
                float dG = g -gAfter;
                float dB = b - bAfter;

                //shift colors towards visible spectrum
                float rMap = 0;
                float gMap = 0.7f*dR + dG;
                float bMap = 0.7f*dR+dB;

                //final
                int rF = (int) Math.abs(r + rMap);
                int gR = (int) Math.abs(g + gMap);
                int bR = (int) Math.abs(b + bMap);
//                Color c = Color.valueOf(rF,gR,bR,a);
//                color = c.toArgb();
                color = Color.rgb(rF,gR, bR);
                imageBitmap.setPixel(x,y,color);

                if(x==240){
                    System.out.println(a/255 +" "+rF+" "+gR);
                }

            }


        }
    }

}
